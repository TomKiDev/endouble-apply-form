<?php
/* Page settings
---------------------------*/
require_once('../../Library/settings/init.php');
/* Set page variables
---------------------------*/
$page_title = 'Purchasing Assistant Vacancy';
$hero_title = 'Purchasing assistant';
$hero_secondary = 'Amsterdam, The Netherlands';

/* Require header
---------------------------*/
require_once( PARTS . '/header.php');

/* Thank yoo body
---------------------------*/
?>
<article class="thankYou container">
  <header>
    <h2>Hey <?php if(isset($_SESSION['name'])) { echo ucfirst($_SESSION['name']);}?></h2>
    <p class="fas fa-grin-beam"></p>
    <p>Thank you for applying for the position</p>
    <p><?php if(isset(($_SESSION['job']))) { echo "of " . $_SESSION['job'];}?></p>
  </header>
  <section class="message">
    <p>We will contact you within 48 hours to discuss your application.</p>
  </section>


  <?php
    // If Send copy was selected an email will be sent message is showen
    if ( (isset($_SESSION['sendCopy'])) && ($_SESSION['sendCopy'] == 1) ) :?>
    <p>A copy of your application was sent to:</p>
    <p class="copy"><?php echo $_SESSION['email'] ?></p>
  <?php endif; ?>
</article>
<?php

// Test area

// // Check input
// echo "<section class='container'><h3>Input</h3><table class='applyFormOutput'>";
// echo "<tr><th>Name</th><th>input</th></tr>";
// foreach ($applyInput as $key => $value) {
//   echo "<tr><td>$key</td><td>: $value</td></tr>";
// }
// echo "</table></section>";
// echo "<h3>" . count($errors) . "</h3>";
//
// // Check errors
// echo "<section class='container'><h3>Errors</h3><table class='applyFormOutput'>";
// echo "<tr><th>Name</th><th>input</th></tr>";
// foreach ($errors as $key => $value) {
//   echo "<tr><td>$key</td><td>: $value</td></tr>";
// }
// echo "</table></section>";
//
// // Check files
// echo "<section class='container'><h3>Files</h3><table class='applyFormOutput'>";
// echo "<tr><th>Name</th><th>input</th></tr>";
// foreach ($_FILES as $key => $value) {
//   foreach ($value as $name => $arr) {
//     echo "<tr><td>$key : $name</td><td>: $arr</td></tr>";
//   }
//   // echo "<tr><td>$key</td><td>: $value</td></tr>";
// }
// echo "</table></section>";

/* Require footer
---------------------------*/
require_once( PARTS . '/footer.php');
?>
