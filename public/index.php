<?php
/* Page settings
---------------------------*/
require_once('../Library/settings/init.php');
/* Set page variables
---------------------------*/
$page_title = 'Purchasing Assistant Vacancy';
$hero_title = 'Purchasing assistant';
$hero_secondary = 'Amsterdam, The Netherlands';
$apply_title = lcfirst(str_replace(" ", "", ucwords($hero_title)));

/* Require header
---------------------------*/
require_once( PARTS . '/header.php');

/* Require parts body of home page
---------------------------*/
require_once( PARTS . '/hero.php');
require_once( PARTS . '/applyForm.php');
require_once( PARTS . '/applyProcedure.php');

/* Require footer
---------------------------*/
require_once( PARTS . '/footer.php')
?>
