<?php
/* Start session if not existing
------------------------------------------*/
if ( ! session_id() ) {
    session_start();
}

/* Show all errors
------------------------------------------*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

ini_get("allow_url_include");
ini_set("allow_url_include", "1");

/* Define root and constants
------------------------------------------*/

//192.168.10.10 endouble.localhost
//http://localhost/~tomerkimhi/Endouble/public/

define("PRIVATE_PATH", dirname(__FILE__));
define("PROJECT_PATH", dirname(PRIVATE_PATH));
define("PUBLIC_PATH", '/Users/tomerkimhi/Sites/Endouble/public');
define("LIB_PATH", PROJECT_PATH);
define("VIEWS", LIB_PATH . '/views');
// define("PAGES", VIEWS . '/pages');
define("PARTS", VIEWS . '/partials');


// $public_end = strpos($_SERVER['SCRIPT_NAME'], '/public') + 7;
// $doc_root = substr($_SERVER['SCRIPT_NAME'], 0, $public_end);
define("WWW_ROOT", '/~tomerkimhi/Endouble/public');
define("IMAGES", WWW_ROOT . '/images');
define("CSS", WWW_ROOT . '/css');
define("JS", WWW_ROOT . '/js');
define("PAGES", WWW_ROOT. '/pages');

// Require all function
require_once(LIB_PATH. '/functions/functions.php');
