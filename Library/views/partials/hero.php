<!-- Hero image
Requires hero title and hero seconday variables-->
<section class="hero">
  <div class="container">
    <h1><?php if(isset($hero_title)) { echo $hero_title; } ?></h1>
    <h2><?php if(isset($hero_secondary)) { echo $hero_secondary; } ?></h2>
  </div>
</section>
