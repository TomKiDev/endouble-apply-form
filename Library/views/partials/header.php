<!-- Header
======================-->
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<!--
TTTTTTTTTTTTTTTT    OOOOO     MMM       MMM   KKK    KKK   IIIIIII
TTTTTTTTTTTTTTTT   OOOOOOO    MMMM     MMMM   KKK   KKK    IIIIIII
     TTTT         OO     OO   MMMM     MMMM   KKK  KKK       III
     TTTT         OO     OO   MMMMM   MMMMM   KKK KKK        III
     TTTT         OO     OO   MMM MMMMM MMM   KKKKKK         III
     TTTT         OO     OO   MMM MMMMM MMM   KKKKKK         III
     TTTT         OO     OO   MMM MMMMM MMM   KKK KKK        III
     TTTT         OO     OO   MMM  MMM  MMM   KKK  KKK       III
     TTTT          OOOOOOO    MMM  MMM  MMM   KKK   KKK    IIIIIII
     TTTT            OOO      MMM   M   MMM   KKK    KKK   IIIIIII

This website was made by TomKi
-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0, user-scalable=yes">
<meta name="description" content="Apply for Purchasing Assistant position at Netwerven">
<title>Netwerven <?php if(isset($page_title)) { echo '- ' . $page_title; } ?></title>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src ="<?php echo JS ?>/bundle.js"></script>
<link href="<?php echo CSS ?>/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <!-- Header -->
  <header>
    <section class="navbar">
      <div class="navContainer">
        <nav id="slideNavigation">
          <span id="closeNav" class="fas fa-times"></span>
          <ul>
            <li><a href="#" class="">Home</a></li>
            <li><a href="#" class="active">Jobs</a></li>
            <li><a href="#" class="">About</a></li>
            <li><a href="#" class="">Departments</a></li>
            <li><a href="#" class="">Contact</a></li>
          </ul>
        </nav>
        <span id="openMenu" class="fas fa-bars" ></span>
        <!-- Logo -->
        <div id="logo">
          <a href="#">LOGO</a>
        </div>
        <!-- Search field -->
        <form id="search" action="#" method="get">
          <input type="text" placeholder=" search for jobs by keyword" value="">
        </form>
        <!-- Navigation -->
        <nav id="fullNavigation">
          <ul>
            <li><a href="#" class="">Home</a></li>
            <li><a href="#" class="active">Jobs</a></li>
            <li><a href="#" class="">About</a></li>
            <li><a href="#" class="">Departments</a></li>
            <li><a href="#" class="">Contact</a></li>
          </ul>
        </nav>
        <!-- Suitcase -->
        <div id="suitcase">
          <span class="fas fa-suitcase"></span>
          <sub>1</sub>
          <p id="tip">Tip! Click the icon to save your favorite jobs<span class="arrowUp"></span></p>
        </div>
      </div>
    </section>
  </header>
  <main>
