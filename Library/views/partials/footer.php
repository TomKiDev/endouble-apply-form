</main>
<!-- Footer -->
<footer class="footer container">
  <!-- Main Navigation -->
  <section class="mainMenu">
    <h4>Jobs at</h4>
    <ul>
      <li><a href="#">Home</a></li>
      <li><a href="#">Jobs</a></li>
      <li><a href="#">About</a></li>
      <li><a href="#">Departments</a></li>
    </ul>
  </section>
  <!-- Information Navigation -->
  <section class="infoMenu">
    <h4>Information</h4>
    <ul>
      <li><a href="#">F.A.Q</a></li>
      <li><a href="#">Application process</a></li>
      <li><a href="#">Privacy policy</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </section>
  <!-- Social media links -->
  <section class="SocialMenu">
    <h4>Follow us</h4>
    <ul>
      <li><a href="newstletter">Newsletter</a></li>
      <li><a href="facebook">Facebook</a></li>
      <li><a href="instagram">Instagram</a></li>
      <li><a href="pintrest">Pinterest</a></li>
      <li><a href="twitter">Twitter</a></li>
      <li><a href="youtube">YouTube</a></li>
      <li><a href="tumblr">Tumblr</a></li>
      <li><a href="googleplus">Google&plus;</a></li>
      <li><a href="linkedin">LinkedIn</a></li>
    </ul>
  </section>
  <!-- Subscribe link -->
  <section class="subscribe">
    <h4>Job alert</h4>
    <a href="#">Subscribe to our job alert</a>
  </section>
  <a id="backToTop" href="#top" title="Back to top">Back to top <span class="fa fa-chevron-up" aria-hidden="true"></span></a>
</footer>
</body>
</html>
