<!-- Apply form
======================-->
<article class="applyForm container">
  <h2>Apply for the position <?php if(isset($hero_title)) { echo "of "; } ?><span><?php if(isset($hero_title)) { echo $hero_title; } ?></span></h2>
  <form action="<?Php if(!isset($action)) {
    echo htmlspecialchars($_SERVER['PHP_SELF']);} else { echo $action; }?>" enctype="multipart/form-data" method="post">
    <!-- Personal details -->
    <section class="PersonalDetails">
      <h3>Personal details &ast;</h3>
      <?php if(isset($apply_title)) { echo
        "<input type='hidden' name='$apply_title' value='$hero_title' />"; }
      ?>
      <input type="text" name="firstName" value="<?php if(isset($applyInput['firstName'])) { echo $applyInput['firstName']; } ?>" placeholder="First Name" required/>
      <input type="text" name="lastname" value="<?php if(isset($applyInput['lastname'])) { echo $applyInput['lastname']; } ?>" placeholder="Last Name" required/>
      <label for="datepicker"><span>Date of birth: </span><input type="text" name="birthday" value="<?php if(isset($applyInput['birthday'])) { echo $applyInput['birthday']; } ?>" placeholder="yyyy-mm-dd" id="datepicker" required></label>
      <input type="email" name="email" value="<?php if(isset($applyInput['email'])) { echo $applyInput['email']; } ?>" placeholder="Email" required/>
      <input type="tel" name="phoneNumber" value="<?php if(isset($applyInput['phoneNumber'])) { echo $applyInput['phoneNumber']; } ?>" placeholder="Phone Number" required/>
      <input type="text" name="street" value="<?php if(isset($applyInput['street'])) { echo $applyInput['street']; } ?>" placeholder="Street" required/>
      <input type="text" name="houseNumber" value="<?php if(isset($applyInput['houseNumber'])) { echo $applyInput['houseNumber']; } ?>" placeholder="House Number" required/>
      <input type="text" name="city" value="<?php if(isset($applyInput['city'])) { echo $applyInput['city']; } ?>" placeholder="City" required/>
      <input type="text" name="zipcode" value="<?php if(isset($applyInput['zipcode'])) { echo $applyInput['zipcode']; } ?>" placeholder="Zipcode" required/>
      <input type="text" name="country" value="<?php if(isset($applyInput['country'])) { echo $applyInput['country']; } ?>" placeholder="Country" required/>
      <section class="gender">
        <span>Gender:</span>
        <input type="radio" name="gender" value="male" <?php if((isset($applyInput['gender'])) && $applyInput['gender'] =="male") { echo "checked"; } ?>/> <span>Male</span>
        <input type="radio" name="gender" value="female" <?php if((isset($applyInput['gender'])) && $applyInput['gender'] =="female") { echo "checked"; } ?>/> <span>Female</span>
        <input type="radio" name="gender" value="other" <?php if((isset($applyInput['gender'])) && $applyInput['gender'] =="other") { echo "checked"; } ?> required/> <span>Rather not say</span>
      </section>
    </section>
    <!-- Motivation -->
    <section class="motivation">
      <h3>Your motivation &ast;</h3>
      <textarea name="motivation" rows="10" cols="30" placeholder="Lorem ipsum dolor sit amet" required><?php if(isset($applyInput['motivation'])) { echo $applyInput['motivation']; } ?></textarea>
    </section>
    <!-- Attachments -->
    <section class="attachments">
      <h3>Attach your documents <span>(pdf, doc(x), jpg max. 4 Mb)</span></h3>
      <ul>
        <li>
          <span>Resume &ast;</span>
          <label for="resumeDropbox" class="button dropbox">Dropbox
            <input id="resumeDropbox" type="file" accept=".pdf, .doc, .docx, .jpg" name="resume" />
          </label>
          <label for="resumeUpload" class="button upload">Upload
            <input id="resumeUpload" type="file" accept=".pdf, .doc, .docx, .jpg" name="resume" />
          </label>
        </li>
        <li>
          <span>Portfolio</span>
          <label for="portfolioDropbox" class="button dropbox">Dropbox
            <input id="portfolioDropbox" type="file" accept=".pdf, .doc, .docx, .jpg" name="portfolio" />
          </label>
          <label for="portfolioUpload" class="button upload">Upload
            <input id="portfolioUpload" type="file" accept=".pdf, .doc, .docx, .jpg" name="portfolio" />
          </label>
        </li>
        <li>
          <span>Photo</span>
          <label for="photoDropbox" class="button dropbox">Dropbox
            <input id="photoDropbox" type="file" accept=".pdf, .doc, .docx, .jpg" name="photo" />
          </label>
          <label for="photoUpload" class="button upload">Upload
            <input id="photoUpload" type="file" accept=".pdf, .doc, .docx, .jpg, .rtf, .txt" name="photo" />
          </label>
        </li>
      </ul>
    </section>
    <!-- JS errors -->
    <section class="errors">
      <h3>Please fill all required fields in order to continue</h3>
      <ul>

      </ul>
    </section>
    <!-- PHP errors -->
    <?php
    if (count($errors) > 0 ){
      echo "<section class='phpErrors'><h3>Please correct any errors to continue</h3>";
      echo "<ul>";
      foreach ($errors as $key => $value) {
        echo "<li>$value</li>";
      }
      echo "</ul></section>";
    }
    ?>
    
    <!-- Recieve copy of application by mail -->
    <section class="submit">
      <div>
        <label for="sendCopy">
          <input type="hidden" name="sendCopy" value="0" />
          <input type="checkbox" id="sendCopy" name="sendCopy" value="1" <?php if((isset($applySendCopy)) && $applySendCopy =="1") { echo "checked"; } ?> />Send me a copy</label>

        <input id="applySubmit" type="submit" value="Apply for this job" />
      </div>
    </section>
  </form>
  <a href="#">&lt; Back to job description</a>
</article>
