<!-- Apply Procedure -->
<div class="procedureBG">
  <article class="applyProcedure container">
    <!-- Application procedure -->
    <section>
      <h2>Application procedure</h2>
      <ul>
        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
        <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</li>
        <li>Quis autem vel eum iure reprehenderit qui in ea voluptate</li>
        <li>Duis aute irure dolor in reprehenderit in voluptate</li>
      </ul>
    </section>
    <!-- Questions and contact -->
    <section>
      <h2>Got a question?</h2>
      <p>Please contact recruitment</p>
      <p>T. +31 20 123 456 78</p>
      <p>Or <a href="mailto:jobs@netwerven.nl?subject=Apply%20for%20Purchasing%20Assistant%20position%20at%20Netwerven">send us an email</a></p>
    </section>
    <div>
      <p id="readMore">Continue reading</p>
      <!-- Toggle more information -->
      <div class="toggletApplyInformation">
        <section>
          <p>Ultricies mi eget mauris pharetra et ultrices. Massa enim nec dui nunc mattis enim ut. Egestas diam in arcu cursus euismod quis. In mollis nunc sed id semper risus in hendrerit gravida. Velit euismod in pellentesque massa placerat duis ultricies lacus. Egestas sed tempus urna et pharetra pharetra massa massa. Elementum pulvinar etiam non quam. Dignissim suspendisse in est ante. Et malesuada fames ac turpis egestas sed tempus. In massa tempor nec feugiat nisl pretium fusce. Neque sodales ut etiam sit amet nisl.</p>
        </section>
        <section>
          <p>Semper auctor neque vitae tempus quam. Blandit turpis cursus in hac. Malesuada fames ac turpis egestas maecenas pharetra. Diam volutpat commodo sed egestas egestas. Non enim praesent elementum facilisis leo vel fringilla est ullamcorper. Fusce id velit ut tortor. Nullam eget felis eget nunc lobortis mattis aliquam faucibus. Potenti nullam ac tortor vitae purus faucibus. Sit amet cursus sit amet dictum sit. Aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin.</p>
        </section>
        <p id="readLess">Read less &#94;</p>
      </div>
    </div>
  </article>
</div>
