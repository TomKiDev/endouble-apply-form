<?php

// Sanitiza user input
function sanitizeInput($data) {
  $data = htmlspecialchars(stripslashes(trim($data)));
  return $data;
}
