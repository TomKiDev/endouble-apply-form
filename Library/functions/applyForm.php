<?php
// Apply form
require_once( 'generalFunctions.php' );
$action = $_SERVER['PHP_SELF'];
$errors = [];
$missing = [];
$applyInput = [];
$files = ['resume', 'portfolio', 'photo'];
$regex = '';
$error = '';

// check Post for errors
if (isset($_POST['purchasingAssistant'])) {

  foreach ( $_POST as $key => $value ) {
    $key = sanitizeInput($key);
    $value = sanitizeInput($value);
    $applyInput[$key] = $value;
  }
  $_SESSION['name'] = $applyInput['firstName'];
  $_SESSION['job'] = $applyInput['purchasingAssistant'];

  foreach ( $applyInput as $key => $value ) {

    // Text values
    if ( $key == 'firstName' || $key == 'lastname' || $key == 'street' || $key == 'city' || $key == 'country' ) {
      // Set regex and error
      $regex = '/^[a-zA-Z\s]{2,512}$/';
      $error = $key;

      switch ($key) {
        case 'firstName':
          $error = 'First name must be letters only and at least 2 characters long';
          break;
        case 'lastname':
          $error = 'Last name must be letters only and at least 2 characters long';
          break;
        case 'street':
          $error = 'Street must be letters only and at least 2 characters long';
          break;
        case 'city':
          $error = 'City must be letters only and at least 2 characters long';
          break;
        case 'country':
          $error = 'Country must be letters only and at least 2 characters long';
          break;
        default:
          $error = 'Input field must be letters only and at least 2 characters long';
          break;
      }

      if  ( (!preg_match( $regex, $value )) || strlen($value) == 0 ) {
      // if  ( 1 == 1 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // Birthday
    } elseif ( $key == 'birthday' ) {
      $regex = '/^\d{4}-\d{2}-\d{2}$/';
      $error = 'You must be 18 years or old to apply';

      if  ( (!preg_match( $regex, $value )) || strlen($value) == 0 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // Email
    } elseif ( $key == 'email' ) {
      $regex = '/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/';
      $error = 'Please use a valid email';

      if  ( (!preg_match( $regex, $value )) || strlen($value) == 0 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // Phone number
    } elseif ( $key == 'phoneNumber' ) {
      $regex = '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2?([ .-]?)([0-9]{4})/';
      $error = 'Please use a valid Phone number';

      if  ( (!preg_match( $regex, $value )) || strlen($value) == 0 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // House number
    } elseif ( $key == 'houseNumber' ) {
      $regex = '/^\d+[a-zA-Z]*$/';
      $error = 'House number must start with a number';

      if  ( (!preg_match( $regex, $value )) || strlen($value) == 0 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // Postcode area
    } elseif ( $key == 'zipcode' ) {
      $regex = '/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i';
      $error = 'Zipcode must contain 4 digits followed by 2 letters';

      if  ( (!preg_match( $regex, $value )) || strlen($value) == 0 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // Gender radio
    } elseif ( $key == 'gender' ) {
      $error = 'Please advise your gender';

      if  ( strlen($value) == 0 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // Motivation textarea
    } elseif ( $key == 'motivation' ) {
      $regex = '/^[a-zA-Z\s.,\-0-9]{4,512}$/';
      $error = 'Please enter a short text regarding your motivation';

      if  ( (!preg_match( $regex, $value )) || strlen($value) == 0 ) {
        $errors[$key] = $error;
        // array_push($errors, $error);
      }

    // send copy
    } elseif ( ($key == 'sendCopy') && ($value == 1) ) {
      $_SESSION['sendCopy'] = $applyInput['sendCopy'];
      $_SESSION['email'] = $applyInput['email'];

      // Upload file
    } else  {
      foreach($files as $tempName) {
        //( $key == 'resume' || $key == 'portfolio' || $key == 'photo')

        if  ( is_uploaded_file($_FILES[$tempName]['tmp_name']) && $_FILES[$tempName]["size"] > 0  )   {
          $testMsg = $tempName;

          // File information
          $fileName = WWW_ROOT . '/' . $_FILES[$tempName]['name'];
          $fileType = strtolower( pathinfo( $fileName, PATHINFO_EXTENSION ) );
          $fileSize = $_FILES[$tempName]["size"];
          $testMsg = $fileSize;
          $maxFileSize = 32000000;
          $uploadOk  = 1;
          $newFileName = $tempName . '_' . $applyInput['firstName'] . '_' .  $applyInput['lastname'] . '.' . $fileType;

          // Check file type
          if ( ( $uploadOk == 1 ) && ( $fileType != 'pdf' && $fileType != 'doc' && $fileType != 'docx'  && $fileType != 'jpg' && $fileType != 'rtf' && $fileType != 'txt' ) ) {
            $error = "$tempName must be pdf, doc(x), rtf, txt or jpg ";
        		$uploadOk  = 0; // Set upload to not ok
        	}

          // Check file size
          if ( ( $uploadOk == 1 ) && ( $fileSize > $maxFileSize ) ) {
            $error = "$tempName must be less than 4Mb";
        		$uploadOk  = 0; // Set upload to not ok
        	}

          // After all checks are ok
          if ( $uploadOk == 1 ) {
        		move_uploaded_file( $_FILES[$tempName]['tmp_name'], PUBLIC_PATH . '/uploadedFiles/' . $newFileName);
        	} else {
            $errors[$tempName] = $error;
          }
        } elseif ($tempName == 'resume') {
          $error = 'Please submit your resume, a pdf, doc(x), rtf, txt or jpg up to 4Mb';
        }

      }
    }

    if (count($errors) <= 0 ) {
      // $action = PAGES . "/thankyou.php";
      header("location: ". PAGES . "/thankyou.php");
    }
  }
};
