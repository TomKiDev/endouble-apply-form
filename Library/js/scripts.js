$( function() {
  const JSPAGES = '/~tomerkimhi/Endouble/Library/views/pages';


  // Navigation
  //============================
  // Slide navigation
  $("#openMenu").click(function() {
    $("#slideNavigation").css({"width" : "105vw", "opacity" : "1"});
  });
  $("#closeNav").click(function() {
    $("#slideNavigation").css({"width" : "0", "opacity" : "0"});
  });
  // Hide full navigation for smaller screens
  if( $(window).width() < 374) {
    $("#fullNavigation").css("display", "none");
  }
  // While testing resize
  $(window).resize( function() {
    if($(window).width() < 374) {
      $("#fullNavigation").css("display", "none");
    } else {
      $("#fullNavigation").css("display", "block");
    }
  });

  // Search form
  //============================
  if(!$("#search")) {
    return false;
  } else {
    let $search = $("#search input");
    // On keyup change adjust css
    $search.keyup( function() {
      let value = $(this).val();
      if (value.length == "0") {
        $search.css("text-transform", "uppercase");
      } else {
        $search.css("text-transform", "none");
      }
      // Check the value
      // console.log( value);
    });
  }

  // Suitcase
  //============================
  // hide the tip
  $("#tip").hide();
  // Show tip on focus
  $("#suitcase").on("mouseenter focus", function() {
    // console.log("you entered the suitcase");
    $("#tip").slideDown("slow").animate(
      { opacity : 1},
      { queue: false, duration: 'slow'}
    );

  });
  // Hide the tip on leave
  $("#suitcase").on("mouseout blur", function() {
    // console.log("you exit the suitcase");
    $("#tip").fadeOut("slow");
  });

  // Apply form
  //============================
  if(!$(".applyForm")) {
    return false;
  } else {
    // Errors div
    let errorsDiv = $(".errors")
    let errorsList = $(".errors ul");
    errorsDiv.css("display", "none");
    // Validate required input fields
    let inputFields = [
      "input:text[name=\'firstName\']",
      "input:text[name=\'lastname\']", "input:text[name=\'birthday\']", "input[type=email][name=\'email\']", "input[type=tel][name=\'phoneNumber\']",
      "input:text[name=\'street\']",
      "input:text[name=\'houseNumber\']",
      "input:text[name=\'city\']",
      "input:text[name=\'zipcode\']",
      "input:text[name=\'country\']",
      "input:radio[name=\'gender\']",
      "textarea[name=\'motivation\']",
      "input:file[name=\'resume\']"
    ];
    // Errors array
    let errors = [];
    let errorsLength = Object.entries(errors).length;
    // console.log("errors is " + errorsLength + " long");

    // Prevent default of submit button
    $("#applySubmit").click( function(e) {

      // console.log("errors is " + errors.length + " long");
      inputFields.forEach( function( input ) {
        $(input).trigger("change");
      });
      let validInputs = allValid(inputFields);
      // console.log("validInputs are: " + validInputs);
      if (validInputs !== true) {
        e.preventDefault();
        // window.location.href= JSPAGES + "/thankyou.php";
      }
    });

    // Check if all required fields are filled
    // Requires array of fields.
    function findName(input) {
      // Find name of input for validation.
      let first = input.indexOf("\'");
      // console.log("first quate index: " + first);
      let name = input.substring(first+1);
      // console.log("after first cut: " + name);
      name = name.replace("\'", "");
      return name = name.replace("]", "");
    }

    // Show or hide errors
    function allValid(inputFields){
      let requiredInputs = 0;
      inputFields.forEach( function( input ) {
        let $this = $(input);


        name = findName(input)
        // console.log(name);
        if ( $this.hasClass('valid') ) {
          // console.log(name + " is valid")
          requiredInputs++;
        } else if (name == "gender" ) {
          let section = $this.parent("section");
          if ( section.hasClass('valid') ) {
            // console.log(name + " is valid")
            requiredInputs++;
          }
        } else if (name == "resume"){
          let section = $this.parents(".attachments");
          // console.log(name);
          if ( section.hasClass('valid') ) {
            // console.log(name + " is valid")
            requiredInputs++;
          }
        } else {
          // console.log(name + " not valid")
          requiredInputs--;
        }


      });

      // console.log("there are " + requiredInputs + "requiredInputs are: ");
      // console.log(requiredInputs);
      // console.log("there are " + (inputFields.length - 1 ) + "inputFields are: ");
      // console.log(inputFields);
      // console.log(requiredInputs)
      // return requiredInputs;
      // console.log("required fields: " + inputFields.length + " Completed: " + requiredInputs );

      // Hide or show errors
      if (requiredInputs == inputFields.length ) {
        errorsDiv.css("display", "none");
        return true;
      } else {
        errorsDiv.css("display", "block");
        return false;
      }
    }

    // Check validation and set class foor currect feedback
    // Requires input selector and valid
    function checkValidation($this, valid) {
      // console.log('it is ' + valid )
      if (valid) {
        $this.removeClass('notValid');
        $this.addClass('valid');
      } else {
        $this.removeClass('valid');
        $this.addClass('notValid');
      }
    };

    inputFields.forEach( function( input ) {
      // Check which input
      // console.log(input);

      // On any change check the value and set errors.
      $(input).on("blur change", function validateInput() {
        let $this = $(this);
        let value = $this.val();
        let regex = "";
        let valid = false;
        let name  = findName(input);
        let error ="";
        let thisId = "#" + name + "Error";
        // console.log(name);
        // console.log("this: " + $this);

        allValid(inputFields);

        // Text values
        if ((name == "firstName") ||
            (name == "lastname") ||
            (name == "street") ||
            (name == "city") ||
            (name == "country")){
          regex = /^[a-zA-Z\s]{2,512}$/
          valid = regex.test(value);
          if (!valid) {
            switch (name) {
              case "firstName":
                error = "<li id=\"" + name + "Error\">First name must be letters only and at least 2 characters long</li>";
                break;
              case "lastname":
                error = "<li id=\"" + name + "Error\">Last name must be letters only and at least 2 characters long</li>";
                break;
              case "street":
                error = "<li id=\"" + name + "Error\">Street must be letters only and at least 2 characters long</li>";
                break;
              case "city":
                error = "<li id=\"" + name + "Error\">City must be letters only and at least 2 characters long</li>";
                break;
              case "country":
                error = "<li id=\"" + name + "Error\">Country must be letters only and at least 2 characters long</li>";
                break;
              default:
                error = "<li id=\"" + name + "Error\">Input field must be letters only and at least 2 characters long</li>";
                break;
            }
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          // console.log("errors is " + errors.length + " long");
          checkValidation($this, valid);
          // console.log("value is: " + value + " valid: " + valid);

          // Birthday
        } else if (name == "birthday") {
          // console.log("birthday");
          regex = /^\d{4}-\d{2}-\d{2}$/;
          valid = regex.test(value);
          if (!valid) {
            error = "<li id=\"" + name + "Error\">You must be 18 years or old to apply</li>";
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          checkValidation($this, valid);
          // console.log("value is: " + value + " valid: " + valid);

          // Email
        } else if (name == "email") {
          let atIndex = value.indexOf("@");
          let pointLastIndex = value.lastIndexOf(".");
          // let $length = value.length;
          regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          valid = regex.test(value);
          if (!valid) {
            error = "<li id=\"" + name + "Error\">Please use a valid email</li>";
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          checkValidation($this, valid);
          // console.log("atIndex: " + atIndex + " pointLastIndex: " + pointLastIndex + " length: " + length + " valid: " + valid);

          // Phone number
        } else if (name == "phoneNumber") {
          regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2?([ .-]?)([0-9]{4})/;
          valid = regex.test(value);
          if (!valid) {
            error = "<li id=\"" + name + "Error\">Please use a valid Phone number</li>";
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          checkValidation($this, valid);
          // console.log("value is: " + value + " valid: " + valid);

          // House number
        } else if (name == "houseNumber") {
          regex = /^\d+[a-zA-Z]*$/;
          valid = regex.test(value);
          if (!valid) {
            error = "<li id=\"" + name + "Error\">House number must start with a number</li>";
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          checkValidation($this, valid);
          // console.log("value is: " + value + " valid: " + valid);

          // Postcode area
        } else if (name == "zipcode") {
          regex = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
          valid = regex.test(value);
          if (!valid) {
            error = "<li id=\"" + name + "Error\">Zipcode must contain 4 digits followed by 2 letters</li>";
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          checkValidation($this, valid);
          // console.log("value is: " + value + " valid: " + valid);

          // Gender radio
        } else if (name == "gender") {
          // console.log("checked input: " + $("input[name=gender]:checked").length);
          if ($("input[name=gender]:checked").length > 0 ) {
            valid = true;
            // console.log(name + " was checked");
          }
          let section = $this.parent("section");
          if (!valid) {
            error = "<li id=\"" + name + "Error\">Please advise your gender</li>";
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          checkValidation(section, valid);
          // console.log("value is: " + value + " valid: " + valid + " parent is: " + section );

          // Motivation textarea
        } else if (name == "motivation") {
          regex = /^[a-zA-Z\s.,\-0-9]{4,512}$/;
          valid = regex.test(value);
          if (!valid) {
            error = "<li id=\"" + name + "Error\">Please enter a short text regarding your motivation</li>";
            errorsList.find(thisId).remove();
            errorsList.append(error);
          } else if (valid) {
            errorsList.find(thisId).remove();
          }
          // console.log(errors);
          checkValidation($this, valid);
          // console.log("value is: " + value + " valid: " + valid);

          // Upload resume
        } else if (name == "resume") {
            let validFileExtensions = [".pdf", ".doc", ".docx", ".rtf", ".txt", ".jpg"];
            let maxFileSize = 32000000;
            let selector = $("#resumeUpload");
            let validFileType = false;
            let validFileSize = false;
            // console.log("id is " +$this.attr('id'));

            if ( $this.attr('id') == $("#resumeUpload")) {
              selector = $("#resumeUpload");
            } else if ( $this.attr('id') == $("#resumeDropbox")) {
              selector = $("#resumeDropbox");
            }

            // Get file size
            let files = selector.prop("files");
            // console.log("files: " + files);
            let fileName = $.map(files, function(val) { return val.name});
            // console.log("fileName: " + fileName);
            let fileSize = $.map(files, function(val) { return val.size});
            // console.log("fileSize: " + fileSize);

            // Check if file size is under max allowed
            // console.log("before upload validFileSize: " + validFileSize)
            if (( fileSize < maxFileSize ) && ( fileSize != 0 ))  {
              validFileSize = true;
              // console.log("after upload validFileSize: " + validFileSize)
            }

            // Check valid file type
            function validType(value, selector) {
              // console.log("value is: "+ value + " the length is: " + value.length + " the size is: " + value.size)
              // console.log("selector is: "+ selector.files + " the length is: " + selector.files.length + " the size is: " + selector.files.size)
              if( value <= 0) {
                // console.log("bad file type")
                return validFileType = false;
              } else {
                for( let i = 0; i < validFileExtensions.length; i++) {
                  let checkExtension = validFileExtensions[i];
                  if(value.substr( value.length - checkExtension.length, checkExtension.length ).toLowerCase() == checkExtension.toLowerCase() ) {
                    // console.log("good file type")
                    return validFileType = true;
                  }
                }
              }
            };
            validType(value, selector);
            // console.log("validFileType: " + validFileType)
            // console.log("validFileSize: " + validFileSize)

            if ( (validFileSize === true) && (validFileType === true) ) {
              // console.log("File is good size and good type");
              valid = true;
            }
            let section = $this.parents(".attachments");
            // console.log("section parent is: " + section.attr('class'));
            // console.log("valid is " + valid);
            if (!valid) {
              error = "<li id=\"" + name + "Error\">Please upload your resume, only pdf, doc(x), rtf, txt or jpg and max 4Mb</li>";
              // console.log("error in file upload");
              errorsList.find("#resumeError").remove();
              errorsList.append(error);
            } else if (valid) {
              errorsList.find("#resumeError").remove();
            }
            // console.log(errors);
            checkValidation(section, valid);
          } else if (value.length > 0 ) {
            // console.log($this + " has no check");
            valid = true;
          }
      } );
    });
  }

  // Datepicker function
  //============================
  $( "#datepicker" ).datepicker( {
    changeMonth: true,
    changeYear: true,
    showOtherMonths: true,
    dateFormat: "yy-mm-dd",
    // Set range to 18 years old to 75 years old
    yearRange: "-75:-18",
    // Set date formet to Day month year
    defaultDate: "-18y-m-d"
  });
  // Slide down animation
  $( "#datepicker" ).datepicker( "option", "showAnim", "slideDown" );

  // Apply procedure more information
  //============================
  // Read more
  $( "#readMore" ).click(function() {
    $(".toggletApplyInformation").slideDown("slow");
  });
  // Read less
  $( "#readLess" ).click(function() {
    $(".toggletApplyInformation").slideUp("slow");
  });

  // Back to top
  //============================
  let $backToTop = $("#backToTop");
	$backToTop.hide(); // Hide the back to top button
	//console.log($(window).scrollTop());
  // Scroll up animation
	$backToTop.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	$(window).on("scroll", function(){  // Begin event scroll window
		if ($(this).scrollTop() > 100) {
			if(!$backToTop.is(":visible")) {
				$backToTop.fadeIn(); // Show the button
			}
		} else {
				$backToTop.fadeOut(); // Hide the button
			}
	});
} );

import style from "../scss/style.scss";
