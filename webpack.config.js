const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== 'production';
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const OptimizaCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
	entry: "./Library/js/scripts.js",
	output: {
		filename: "bundle.js",
		path: path.join(__dirname, "public/js")
	},
	module: {
		rules: [
			{
			test: /\.js$/,
			exclude: /(node_modules)/,
			use: {
				loader: "babel-loader",
					options: {
						presets: ["env"]
					}
				}
			},
			{
			test: /\.s?[ac]ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { url: false, sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ],
			},
			{
				test: /\.jpg$/,
				use: [
					{ loader: "url-loader"}
				]
			},
			{
				test: /\.png$/,
				use: [
					{ loader: "url-loader"}
				]
			}
		]
	},
	plugins: [
    new MiniCssExtractPlugin({
      filename: "../../public/css/style.css"
    }),
		new UglifyJSPlugin(),
		new OptimizaCSSAssetsPlugin({
			assetNameRegExp: /\.css$/g,
			cssProcessor: require('cssnano'),
			cssProcessorOptions : { discardComments: {removeAll: true } },
			canPrint: true
		})
  ],
	mode : devMode ? 'development' : 'production'
}
